﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace csharp.Tests
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        [TestCase(50, 49)]
        [TestCase(1, 0)]
        [TestCase(0, 0)]
        public void UpdateQuality_NormalItem_QualityDecreasedByOne(int itemQuality, int expectedQuality)
        {
            var item = new Item
            {
                Name = "foo",
                SellIn = 1,
                Quality = itemQuality
            };

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase(50, 48)]
        [TestCase(1, 0)]
        [TestCase(0, 0)]
        public void UpdateQuality_ConjuredNormalItem_QualityDecreasedBytwo(int itemQuality, int expectedQuality)
        {
            var item = new Item
            {
                Name = "Conjured foo",
                SellIn = 1,
                Quality = itemQuality
            };

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase(50, 48)]
        [TestCase(2, 0)]
        [TestCase(0, 0)]
        public void UpdateQuality_NormalItemSellInZero_QualityDecreasedByTwo(int itemQuality, int expectedQuality)
        {
            var item = new Item
            {
                Name = "foo",
                SellIn = 0,
                Quality = itemQuality
            };

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase(50, 20, 21)]
        [TestCase(11, 20, 21)]
        [TestCase(10, 20, 22)]
        [TestCase(9, 20, 22)]
        [TestCase(6, 20, 22)]
        [TestCase(5, 20, 23)]
        [TestCase(4, 20, 23)]
        [TestCase(0, 20, 0)]
        public void UpdateQuality_BackstagePass_QualityIncreases(int ItemSellIn, int itemQuality, int expectedQuality)
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = ItemSellIn,
                Quality = itemQuality
            };

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase(20, 21)]
        public void UpdateQuality_AgedBrie_QualityIncreases(int itemQuality, int expectedQuality)
        {
            var item = new Item
            {
                Name = "Aged Brie",
                SellIn = 2,
                Quality = itemQuality
            };

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase("Aged Brie")]
        [TestCase("Backstage passes to a TAFKAL80ETC concert")]
        public void UpdateQuality_QualityOfItem_Max50(string itemName)
        {
            var item = new Item
            {
                Name = itemName,
                SellIn = 2,
                Quality = 50
            };

            var expectedQuality = 50;

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

        [Test]
        [TestCase(0)]
        [TestCase(5)]
        [TestCase(100)]
        public void UpdateQuality_Sulfuras_NoDecreaseInQuality(int itemSellIn)
        {
            var item = new Item
            {
                Name = "Sulfuras, Hand of Ragnaros",
                SellIn = itemSellIn,
                Quality = 80
            };

            var expectedQuality = 80;

            IList<Item> Items = new List<Item>
            {
                item
            };

            var subject = new GildedRose(Items);
            subject.UpdateQuality();

            var items = subject.Items;

            var updatedItem = items.FirstOrDefault(f => f.Name == item.Name);
            Assert.IsNotNull(updatedItem);

            Assert.AreEqual(item.SellIn, updatedItem.SellIn);
            Assert.AreEqual(expectedQuality, Items[0].Quality);
        }

    }
}
